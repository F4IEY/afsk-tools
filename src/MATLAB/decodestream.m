%Script pour decoder en temps réel sur un support hardware(carte son)
audiosource = audiorecorder();
mStart = 1;
wavMatrix = [];
initS = 1; %pour commencer le scan
%il faut enregistrer l'audio et mettre un marqueur de début/fin
record(audiosource);
pause(0.1); %on laisse enregisrer 100ms au départ
while 1
    frameStream = getaudiodata(audiosource);
    for i=initS:length(frameStream)
        if i > 1
          pastPeak = abs(frameStream(i-1));
        endif
        peak = abs(frameStream(i));
        if mStart
            if peak < 0.2
               continue
            else
                mStart =  0;
                %i = i - 1;
            endif
        endif
        wavMatrix = [wavMatrix frameStream(i)];
        if mStart == 0 && peak < 0.2 && pastPeak < 0.2
            stop(audiosource);
            break
        endif
    end
    %on se prépare à scanner la partie suivante
    if length(frameStream) > initS
        initS = length(frameStream);
    endif
    %Une fois le record terminé, on process
    if ~isrecording(audiosource);
        %on prend le wav et on le met a décoder
        audiowrite('stream.wav', wavMatrix, audiosource.SampleRate);
        wavMatrix = []; %on supprime le contenu de la matrice d'analyse
        clear frameStream;
        afsk_decode('stream.wav', 1200);
        %on relance immédiatement l'enregistrement
        pause(1);
        record(audiosource);
    endif
end