%Générateur de trame AFSK pour des applications de communication numérique
%(aprs) ©2020 F4IEY, Efflam
%cet exemple permet d'écouter le texte en ASCII converti en WAV avec la
%fonction getSoundMatrix() puis de décoder le fichier
clear all;
trame = getSoundMatrix('RYRYRYRYRY 73 DE F4IEY', 1200);
audiowrite('test_AFSK_1200.wav', trame, 48000);
soundsc(trame, 48000);
texte = afsk_decode('test_AFSK_1200.wav', 1200);

