function decodedMessage = afsk_decode(wavFile, baudrate)
    %Permet de décoder l'AFSK directement en texte ASCII
    %©2020 Jules F4IEY
    %on définit le baudrate pour la décomposition du signal
    risingEdge = 1/baudrate;
    epsilon = 200; %+-200Hz
    %transitionArray = zeros(1, 2); %on déclare un array de taille 2
    %On récupère les données
    [wavMatrix, Fe] = audioread(wavFile);
    %on va parcourir le signal obtenu pour identifier les space et les mark
    %(codage NRZI)
    %pour l'encodage, on utilise la méthode NRZI avec deux signaux:
    mark = synthad(0.01, 1200, risingEdge, Fe); %la sinus mark
    space = synthad(0.01, 2200, risingEdge, Fe); %la sinus space
    decodedBinary = '';
    decodedMessage = '';
    prevMarkSpace = 'init';
    for i=1:length(mark):length(wavMatrix)
        if i == 1
           while abs(wavMatrix(i)) < 0.2
               i = i + 1;
           end
        end
        if length(wavMatrix) - i < length(mark) - 1
            break
        end
        isMarkSpace = '';
        %on vient "détecter" la fréquence pour voir si c'est un mark ou un
        %space
        fftWav = fft(wavMatrix(i:i-1+length(mark)));
        [~,locs] = max(abs(fftWav(1:length(fftWav)-1)));
        qrg = 0:(Fe/length(mark)):(Fe/2); %on a la fréquence cherchée a qrg(maxPeak)
        detected = int64(qrg(locs));
        if detected > 1200 - epsilon && detected < 1200 + epsilon
           %c'est donc un mark
           isMarkSpace = 'm';
        elseif detected > 2200 - epsilon && detected < 2200 + epsilon
            %c'est un space
            isMarkSpace = 's';
        end
        if strcmp(prevMarkSpace, 'init')
            prevMarkSpace = isMarkSpace;
        else
            transitionArray = strcat(prevMarkSpace, isMarkSpace);
            if strcmp(transitionArray, 'mm') || strcmp(transitionArray, 'ss')
                decodedBinary = strcat(decodedBinary, '1');
            elseif strcmp(transitionArray, 'ms') || strcmp(transitionArray, 'sm')
                decodedBinary = strcat(decodedBinary, '0');
            end
            prevMarkSpace = isMarkSpace;
        end
    end
%ici, decodedBinary contient le message, il faut maintenant le traduire en
%ASCII
    %On affiche directement le message en sortie de console
    decodedMessage = char(bin2dec(reshape(decodedBinary,8,[]).')).'
end