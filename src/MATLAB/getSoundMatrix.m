function wavMatrix = getSoundMatrix(message, baudrate)
    %génère la matrice audio AFSK pour le message rentré en paramètre à la
    %vitesse souhaitée (en baud)
    %©2020 Jules (F4IEY), Iyed (Iyenal), Efflam (efflam), Nico (Wizado)
    %tout d'abord quelques paramètres...
    %sync = 'RYRYRYRYRY'; %on envoie 5 fois RY au début de trame pour être sur d'être synchronisé
    %message = strcat(sync, message);
    wavMatrix = []; %la matrice de sortie
    Fe = 48000; %on sample à 48000Hz pour restituer au mieux les signaux
    risingEdge = 1/baudrate; %pour la vitesse de transmission (en baud)
    %pour l'encodage, on utilise la méthode NRZI avec deux signaux:
    mark = synthad(0.01, 1200, risingEdge, Fe); %la sinus mark
    space = synthad(0.01, 2200, risingEdge, Fe); %la sinus space
    %on convertit chaque caractrère du message en ASCII
    doublemessage = double(message);
    toSend = [];
    %puis on le convertit en binaire brut
    for j=1:length(doublemessage)
        toSend = [toSend dec2bin(doublemessage(j), 8)];
    end
    %codage NRZI pour chaque bit: 0 -> alternance mark/space, 1 ->
    %space/space ou mark/mark
    markShift = length(mark) - 1;
    spaceShift = length(space) - 1;
    for k=1:length(toSend)
            str = toSend(k);
           if str == '0'
               if k == 1
                   %initialisation
                    wavMatrix = [wavMatrix space];
                    wavMatrix = [wavMatrix mark];
                    'init'
               elseif wavMatrix((length(wavMatrix) - markShift):length(wavMatrix)) == mark
                   %si la dernière sinus était un mark
                    wavMatrix = [wavMatrix space];
                    'space'
               else
                   wavMatrix = [wavMatrix mark];
                   'mark'
               end     
           else
               %de même ici
               if k == 1
                    wavMatrix = [wavMatrix space];
                    wavMatrix = [wavMatrix space];
                    'init1'
               elseif wavMatrix((length(wavMatrix) - spaceShift):length(wavMatrix)) == space
                   wavMatrix = [wavMatrix space];
                   'space'
               else
                   wavMatrix = [wavMatrix mark];
                   'mark'
               end    
           end
    end   
    
end

