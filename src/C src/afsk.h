//Fichier HEADER
#ifndef AFSK_H
#define AFSK_H
sig afsk_encode(char* message, const int baudrate, unsigned int* taille);
char* toBin(char* ascii);
char* afsk_decode(sig wav, const int baudrate);
#endif
