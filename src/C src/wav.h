//Fichier HEADER
#ifndef WAV_H
#define WAV_H
typedef int32_t* sig;
sig synthad(double a, double f, double T, unsigned int Fe, unsigned int* taille);
void wavwrite(char* filename, sig dataIn, unsigned int Fe, unsigned int* taille);
sig appends(sig dest, sig from, unsigned int* tailled, unsigned int* taillef);
int sigcmp(sig s1, sig s2, unsigned int* taille1, unsigned int* taille2); //renvoie 0 si les signaux sont identiques
sig subsig(sig s, unsigned int min, unsigned int max, unsigned int* taille); //extrait un signal
#endif
