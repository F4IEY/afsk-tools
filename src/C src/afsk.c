#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
#include <string.h>
#include "wav.h"
#include "afsk.h"



char* toBin(char* ascii){
    int i;
    char* bin = malloc(strlen(ascii)*8 + 1);
    for(i=0; i<strlen(ascii); i++){
        int conv = (int)ascii[i]; //conversion décimale
        bin += 8;
        int j;
        for(j=0; j<8; j++){
            --bin;
            *bin = conv % 2 + '0'; //conversion binaire
            conv = conv / 2;
        }
        bin += 8;
    }
    *bin = '\0';
    bin -= strlen(ascii)*8;
    return bin;
}

sig afsk_encode(char* message, const int baudrate, unsigned int* pwm){
    sig wavMatrix;
    unsigned int Fe = 48000;
    double risingEdge = 1/(double)baudrate;
    unsigned int tMark, tSpace = 0;
    unsigned int* pMark = &tMark;
    unsigned int* pSpace = &tSpace;
    sig mark = synthad(1, 1200, risingEdge, Fe, pMark);
    sig space = synthad(1, 2200, risingEdge, Fe, pSpace);
    int markShift = tMark - 1;
    int spaceShift = tSpace - 1;
    unsigned int subt = 0;
    unsigned int* subp = &subt;
    //conversion du message ASCII en binaire
    char* toSend = toBin(message);
    //NRZI:
    int k;
    for(k = 0; k<strlen(toSend); k++){
        //on encode
        char str = toSend[k];
        if(str == '0'){
            if(k==0){
                //init space mark
                wavMatrix = appends(wavMatrix, space, pwm, pSpace);
                wavMatrix = appends(wavMatrix, mark, pwm, pMark);
            }
            else if(sigcmp(subsig(wavMatrix, (*pwm - 1) - markShift, (*pwm - 1), subp), mark, subp, pMark) == 0) //si c'etait un mark
                wavMatrix = appends(wavMatrix, space, pwm, pSpace);
            else wavMatrix = appends(wavMatrix, mark, pwm, pMark);
        }
        else{
            if(k==0){
                //init space space
                wavMatrix = appends(wavMatrix, space, pwm, pSpace);
                wavMatrix = appends(wavMatrix, space, pwm, pSpace);
            }
            else if(sigcmp(subsig(wavMatrix, (*pwm - 1) - spaceShift, (*pwm - 1), subp), space, subp, pSpace) == 0) //si c'etait un space
                wavMatrix = appends(wavMatrix, space, pwm, pSpace);
            else wavMatrix = appends(wavMatrix, mark, pwm, pMark);
        }
    }
    free(mark);
    free(space);
    return wavMatrix;
}
