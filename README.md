# AFSK Tools

AFSK Tools est un projet consistant à rendre la technologie AFSK *accessible à tous* (et pas uniquement aux radioamateurs).<br>
Ces outils permettent d'encoder et de décoder des messages numériques pour de nombreuses applications.<br>
Pour le moment, *le code source n'est disponible qu'en language* **OCTAVE/MATLAB**. Il sera par la suite porté en langage plus bas niveau (C/C++) pour une meilleure compatibilité.

## Utilisation

Cloner le projet dans un répertoire quelconque que l'on peut ouvrir avec *OCTAVE/MATLAB* avec:
```bash
git clone https://gitlab.com/F4IEY/afsk-tools.git
```
### Exécution sous MATLAB

Ouvrir le dépôt cloné avec *MATLAB/OCTAVE/SCILAB* et faire:
```matlab
clear all
close all
cd src/MATLAB
clibuild
```
Suivez les étapes en rentrant le message et la vitesse de transmission *en bauds* comme indiqué.

### Exécution standard

~~A venir~~